Выполнена только базовая часть тестового задания
Запустить manage.py из дирректории forWork

urls:
'admin/' - стандартная админка Django с настроеннымb функциями изменением всех таблиц.
логин / пароль - user / admin

<int:pk> - ключ записи поле "id"

'api/v1/mail/'
GET - получение всех рассылок
Пример ответа:
    {
        "id": 1,
        "text": "Новое сообщение",
        "date_start": "2022-08-11T17:03:08Z",
        "date_end": "2022-08-12T17:03:15Z",
        "cod_number": 703,
        "teg": "rgr",
        "timezone": "Asia"
    }
POST - добавление новой записи, все поля обязательные
Пример запроса:
    {
        "text": "Новое сообщение",
        "date_start": "2022-08-11T17:03:08Z",
        "date_end": "2022-08-12T17:03:15Z",
        "cod_number": 703,
        "teg": "rgr",
        "timezone": "Asia"
    }

'api/v1/mail/<int:pk>/'
PUT - обновление данных рассылки по ключу
Пример запроса:
    {
        "text": "Новое сообщение",
        "date_start": "2022-08-11T17:03:08Z",
        "date_end": "2022-08-12T17:03:15Z",
        "cod_number": 703,
        "teg": "rgr",
        "timezone": "Asia"
    }
DELETE - удаление рассылки по ключу

'api/v1/user/'
GET - получение всех клиентов
Пример ответа:
    {
        "id": 2,
        "phone": "+77021351015",
        "cod_number": 556,
        "teg": "rgr",
        "timezone": "Asia"
    }
POST - добавление новой записи, все поля обязательные, телефон формата +7ХХХХХХХХХХ (X - цифра от 0 до 9)
Пример запроса:
    {
        "phone": "+77021351015",
        "cod_number": 556,
        "teg": "rgr",
        "timezone": "Asia"
    }

'api/v1/user/<int:pk>/'
PUT - обновление данных клиента по ключу
Пример запроса:
    {
        "phone": "+77021351015",
        "cod_number": 556,
        "teg": "rgr",
        "timezone": "Asia"
    }
DELETE - удаление клиента по ключу

'api/v1/getMessages/'
GET - получения общей статистики по созданным рассылкам и количеству отправленных сообщений по ним с группировкой по статусам
Пример ответа:
    {
        "ID_mail": 1,
        "status": "error",
        "messages": 1
    },
    {
        "ID_mail": 1,
        "status": "ok",
        "messages": 3
    }
POST - получения детальной статистики отправленных сообщений по конкретной рассылке
Пример запроса:
    {
        "ID_mail": 1,
    }
Пример ответа:
    {
        "messages": [
            {
                "ID_mail": 1,
                "status": "error",
                "messages": 1
            },
            {
                "ID_mail": 1,
                "status": "ok",
                "messages": 3
            }
        ],
        "text": "Новое сообщение"
    }