from django.core.validators import RegexValidator
from django.db import models

class mail(models.Model):
    text = models.TextField()
    date_start = models.DateTimeField()
    date_end = models.DateTimeField()
    cod_number = models.IntegerField()
    teg = models.CharField(max_length=20)
    timezone = models.CharField(max_length=20)

    def __str__(self):
        return self.text

class user(models.Model):
    phoneRegex = RegexValidator(regex=r"^\+{1}\d{11}$")
    phone = models.CharField(validators=[phoneRegex], max_length=16, unique=True)
    cod_number = models.IntegerField()
    teg = models.CharField(max_length=20)
    timezone = models.CharField(max_length=20)

class message(models.Model):
    date_create = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=30)
    ID_mail = models.ForeignKey('mail', on_delete=models.PROTECT, null=False)
    ID_user = models.ForeignKey('user', on_delete=models.PROTECT, null=False)

