import datetime
import threading
import time
import requests

from django.db.models import Count
from django.forms import model_to_dict
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import *


class mailApiView(generics.ListCreateAPIView):
    queryset = mail.objects.all()
    serializer_class = mailSerializer

    def post(self, request):
        serializer = mailSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        now = datetime.datetime.now()
        message_to_users = serializer.data['text']

        # Проверка даты начала и окончания в сравнении с датой сейчас

        date_start = datetime.datetime.strptime(serializer.data['date_start'], '%Y-%m-%dT%H:%M:%SZ')
        date_end = datetime.datetime.strptime(serializer.data['date_end'], '%Y-%m-%dT%H:%M:%SZ')

        if date_end > now:
            if date_start < now:
                sec = 0
            else:
                sec = int((date_start - now).total_seconds())
                print(sec)

            # функция в другом потоке для срабатывания через определенное время

            def mysettimeout(a):
                time.sleep(a)
                sql = user.objects.filter(
                    cod_number=serializer.data['cod_number'],
                    teg=serializer.data['teg'],
                    timezone=serializer.data['timezone']
                ).values()
                time.sleep(1)

                # отправка данных на сервер, при возвращении создать запись в базе сообщений

                for i in sql:
                    # обработка события, если дата окончания прошла
                    now_in_for = datetime.datetime.now()
                    if date_end < now_in_for:
                        break
                    try:
                        response = requests.post(
                            url='https://probe.fbrq.cloud/v1/send/' + str(i['id']),
                            json={"id": i['id'], "phone": i['phone'], "text": message_to_users},
                            headers={
                                'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2OTEyNDMwNzUsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6IklJSUFsZXhYeDY2NklJSSJ9.pwsEVGIqzWQCgE3OPIM70khJqcO4vrUDm5bH2pg3cXQ'
                            }
                        )
                        return_status = str(response.json()['message'])
                    except BaseException as e:
                        # обработка ошибок сделана общая, можно расписать
                        return_status = str(e)

                    mes_return = message.objects.create(
                        status=return_status,
                        ID_mail_id=serializer.data['id'],
                        ID_user_id=i['id']
                    )

                    # оставил print записи специально, можно убрать

                    print(model_to_dict(mes_return))

            thr = threading.Thread(target=mysettimeout, args=(sec,))
            thr.start()
        else:
            # можно добавить обработку события, если дата окончания уже прошла
            print('просрочено')

        return Response(serializer.data)


class mailApiUpdate(generics.RetrieveUpdateDestroyAPIView):
    queryset = mail.objects.all()
    serializer_class = mailSerializer


class userApiView(generics.ListCreateAPIView):
    queryset = user.objects.all()
    serializer_class = userSerializer


class userApiUpdate(generics.RetrieveUpdateDestroyAPIView):
    queryset = user.objects.all()
    serializer_class = userSerializer

# получение данных об определенной рассылки

class getMessage(APIView):
    def get(self, request):
        get_list = message.objects.all().values('ID_mail', 'status').annotate(messages=Count('status'))
        return Response(list(get_list))

    def post(self, request):
        get_list = message.objects.all().values('ID_mail', 'status').annotate(messages=Count('status')).filter(
            ID_mail=request.data['ID_mail'])
        text = str(mail.objects.all().filter(id=request.data['ID_mail'])[0])
        return Response({'messages': list(get_list), 'text': text})
