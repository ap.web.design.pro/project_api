from rest_framework import serializers
from .models import *

class mailSerializer(serializers.ModelSerializer):
    class Meta:
        model = mail
        fields = '__all__'

    def create(self, validated_data):
        return mail.objects.create(**validated_data)

class userSerializer(serializers.ModelSerializer):
    class Meta:
        model = user
        fields = '__all__'