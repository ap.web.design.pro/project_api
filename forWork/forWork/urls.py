from django.contrib import admin
from django.urls import path
from myProject.views import *

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/mail/', mailApiView.as_view()),
    path('api/v1/mail/<int:pk>/', mailApiUpdate.as_view()),
    path('api/v1/user/', userApiView.as_view()),
    path('api/v1/user/<int:pk>/', userApiUpdate.as_view()),
    path('api/v1/getMessages/', getMessage.as_view()),
]
